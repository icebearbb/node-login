# node-login

A simple dynamic webpage demonstrating a login using NodeJS and MySQL.

## credits

Initial code found on https://codeshack.io/basic-login-system-nodejs-express-mysql (David Adams)

## required software

* NodeJS
* XAMPP (local) or
* MySQL-Server (remote)

## installation

* Use a shell (e.g. Git bash)
* Clone repo.
* Change into repo directory: 'cd node-login'
* Execute 'npm install'

* If you're using XAMPP locally: 
  * Start Apache/MySQL server (XAMPP).
  * Open address 'localhost/phpmyadmin' in a browser
  * Import 'logindb.sql' into server (creates new database 'nodelogin').
  
* If you're using our remote server:
  * Execute 'mysql -u admin -p < logindb.sql'
  
* Execute 'node login.js'
* Open address 'localhost:10000' in a browser (or <our-server-name>:10000)
* Use credentials test / test to log in.
