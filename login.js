// taken from https://codeshack.io/basic-login-system-nodejs-express-mysql/

var mysql = require('mysql');
var express = require('express');
var session = require('express-session');
var bodyParser = require('body-parser');
var path = require('path');
var crypto = require('crypto');

var dbconfig = require('./dbconf.json');

var connection = mysql.createConnection(dbconfig);

var app = express();

app.use(session({
	secret: 'e7d51f7c-1c79-4983-aee8-a2d54db8049f',
	resave: true,
	saveUninitialized: true
}));

app.use(bodyParser.urlencoded({extended : true}));
app.use(bodyParser.json());

app.get('/', function(request, response) {
	response.sendFile(path.join(__dirname + '/login.html'));
});

app.post('/auth', function(request, response) {
	var username = request.body.username;
	var password = crypto.createHash('sha256').update(request.body.password).digest('hex');
	if (username) {
		connection.query('SELECT * FROM accounts WHERE username = ? AND password = ?', [username, password], function(error, results, fields) {
			if (results.length > 0) {
				request.session.loggedin = true;
				request.session.username = username;
				response.redirect('/home');
			} else {
				response.send('Incorrect Username and/or Password!');
			}			
			response.end();
		});
	} else {
		response.send('Please enter Username and Password!');
		response.end();
	}
});

app.get('/home', function(request, response) {
	if (request.session.loggedin) {
		response.send('Welcome back, ' + request.session.username + '!');
	} else {
		response.redirect('/');
	}
	response.end();
})

app.listen(10000);
console.log('listening on port 10000');
